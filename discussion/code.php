<?php 


// Variables
// Variables are defined using the dollar notation($) before the name of the variable
$name = "John Smith";
$email = "johnsmith@gmail.com";

// Constants
define('PI', 3.1416);

// Data Types

// Strings
$state = "New York";
$country = "United States of America";
// $address = $state.", " .$country; - concatenation thru dot sign
$address = "$state, $country"; //concatenation thru double quotes

// Integers
$age = 31;
$headcount = 26;

// decimal
$grade = 98.2;
$distanceInKilometers = 1324.34;

// boolean
$hasTravelledAbroad = false;
$haveSymptoms = true;

// null
$boyfriend = null;
$girlfriend = null;

// Arrays
$grades = array(90.1, 87, 98, 93);

// Objects
$gradesObj = (object) [
	'firstGrading' => 98.7,
	'secondGrading' => 92.1,
	'thirdGrading' => 90.2,
	'fourthGrading' => 95
];

$personObj = (object) [
	'fullName' => 'John Doe',
	'isMarried' => false,
	'age' => 35,
	'address' => (object) [
		'state' => 'New York',
		'country' => 'United STates of America'
	]
];


// Operators

$x = 1342.14;
$y = 1268.24;

$isLegalAge = true;
$isRegistered = false;

// Functions
function getFullName($firstName = "User", $middleInitial= "User", $lastName = "User"){
	return "$lastName, $firstName, $middleInitial";
}

// Selection Control Structures

//If-ElseIf-Else Statement

function determineTyphoonIntensity($windSpeed) {
	if ($windSpeed <30) {
		return 'Not a typhoon yet';
	} else if ($windSpeed <= 61) {
		return 'Tropical depression detected';
	} else if ($windSpeed >= 62 && $windSpeed <= 88) {
		return 'Tropical storm detected';
	} else if ($windSpeed >= 89 && $windSpeed <= 117) {
		return 'Severe tropical storm detected';
	} else {
		return 'Typhoon detected';
	}
}

// Conditional (Ternary) Operator

function isUnderAge($age) {
	return ($age < 18) ? true : false;
}


// Switch Statement

function determineComputerUser($computerNumber) {
	switch ($computerNumber) {
		case 1:
		return 'Linus Torvalds';
		break;
		case 2:
		return 'Steve Jobs';
		break;			
		case 3:
		return 'Sid Meier';
		break;
		case 4: 
		return 'Onel De Guzman';
		break;			
		case 5: 
		return 'Christian Salvador';
		break;
		default:
		return $computerNumber.' is out of bounds.';
		break;
	}
}

//Try-Catch-Finally Statement
function greeting($str) {
	try {
		//Attempt to execute a code;
		if (gettype($str) == 'String') {
			echo $str;
		} else {
			throw new Exception("Oooops!");
		}
	} 
	catch (Exception $e) {
		echo $e->getMessage();
	}
	finally {
	// continue execution of code regardless of success and failure of code execution
		echo " I did it again!";
	}
}


// ACTIVITY
// 1
function address($details, $city, $province, $country) {
	return $details . ', ' . $city . ', ' . $province . ', ' . $country;
}


// 2
function getLetterGrade($grade) {


	$g = $grade;
	$m = "$g is equivalent to ";

	if ($g >= 98 && $g <= 100) {
		return "$m 'A+'";
	} elseif ($g >= 95 && $g <= 97) {
		return "$m 'A'";
	} elseif ($g >= 92 && $g <= 94) {
		return "$m 'A-'";
	} elseif ($g >= 89 && $g <= 91) {
		return "$m 'B+'";
	} elseif ($g >= 86 && $g <= 88) {
		return "$m 'B'";
	} elseif ($g >= 83 && $g <= 85) {
		return "$m 'B-'";
	} elseif ($g >= 80 && $g <= 82) {
		return "$m 'C+'";
	} elseif ($g >= 77 && $g <= 79) {
		return "$m 'C'";
	} elseif ($g >= 75 && $g <= 76) {
		return "$m 'C-'";
	} else {
		return "$m 'D'";
	}
}







?>