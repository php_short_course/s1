<?php require_once "./code.php" ?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S01: PHP Basics and Selection Control Structures</title>
</head>
<body>
	<!-- <h1>Hello World!</h1> -->

	<h1>Echoing Values</h1>
	<!-- Variables can be used to output the data in double quotes while single quotes do not -->
<!--         <p>
            <?php   echo 'Good day $name! Your given email is $email' ?>
        </p> -->

        <p>
        	<?php   echo "Good day $name! Your given email is $email" ?>
        </p>

        <p>
        	<?php   echo PI ?> <!-- //define -->
        </p>

        <p>
        	<?php   echo "$address" ?>
        </p>

        <h1>Data Types</h1>
        <p>
        	<?php   echo $age ?>
        	<?php   echo $headcount ?>
        </p>

        <!-- To output the value of an object property, the arrow notation can be used -->
        <p>
        	<?php   echo $gradesObj->firstGrading; ?>
        	<?php   echo $personObj->address->state; ?>
        </p>

        <!-- Normal echoing of null and boolean var will not make it visible to the web page -->
        <p><?php echo $hasTravelledAbroad; ?></p>
        <p><?php echo $girlfriend; ?></p>

        <!-- var dump see more details info on the variable -->
        <p><?php echo gettype($hasTravelledAbroad); ?></p>
        <p><?php echo var_dump($hasTravelledAbroad); ?></p>
        <p><?php echo var_dump($girlfriend); ?></p>

        <p><?php echo $grades [2]; ?></p>

        <h1>Operators</h1>

        <h2>Arithmetic Operators</h2>
        <p> Sum: <?php echo $x = $y; ?></p>

        <h2>Equality Operators</h2>
        <p>Loose Equality: <?php var_dump($x == 1342.14); ?></p>
        <p>Loose Equality: <?php var_dump($x != 1342.14); ?></p>


        <h2>Greater/lesser Operators</h2>
        <p>is greater: <?php var_dump($x < $y); ?></p>

        <h2>Logical Operators</h2>
        <p>Are All Requirements Met: <?php echo var_dump($isLegalAge && $isRegistered); ?></p>
        <p>Are All Requirements Met: <?php echo var_dump(!$isLegalAge && !$isRegistered); ?></p>

        <h1> Function </h1>
        <p>Full Name: <?php echo getFullName(); ?></p>

        <h2>If-ElseIf-Else</h2>
        <p><?php echo determineTyphoonIntensity(35); ?></p>

        <h2>Ternary</h2>
        <p>78: <?php echo var_dump(isUnderAge(78)); ?></p>

        <h2>Switch</h2>
        <p><?php echo determineComputerUser(4) ?></p>

        <h2>Try-Catch-Finally</h2>
        <p><?php echo greeting(3) ?></p>

        <hr>
        <h1>ACTIVITY</h1>

        <h4>ADDRESS:</h4>
        <p><?php echo address("3F Casswyn Building, Timog Avenue", "Quezon City", "Metro Manila", "Philippines"); ?></p>
        <p><?php echo address("3F Enzo Building, Buendia Avenue", "Makati City", "Metro Manila", "Philippines"); ?></p>

        <h4>GRADES</h4>
        <p><?php echo getLetterGrade(99); ?></p>
        <p><?php echo getLetterGrade(94); ?></p>
        <p><?php echo getLetterGrade(74); ?></p>




    </body>
    </html>